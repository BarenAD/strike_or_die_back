package com.controllers;

import com.google.gson.Gson;
import com.services.CheckAuthService;
import com.services.GetSaveService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

@ApplicationScoped
public class ControllerGetSave
{
    @Inject
    GetSaveService getSaveService;

    @Inject
    CheckAuthService checkAuthService;

    public Response getSave(String json){
        Gson gson = new Gson();
        ObjectMethodGetSave objectMethodGetSave = gson.fromJson(json, ObjectMethodGetSave.class);
        if (objectMethodGetSave.getId() != null && objectMethodGetSave.getToken() != null) {
            if (checkAuthService.check(objectMethodGetSave.getId(), objectMethodGetSave.getToken())){
                return getSaveService.getSave(objectMethodGetSave.getId());
            }
        }
        return Response.status(401, "UNAUTHORISED").entity("Process error: Invalid token").build();
    }
}


class ObjectMethodGetSave
{
    private Integer id;
    private String token;

    public String getToken() {
        return token;
    }
    public Integer getId() {
        return id;
    }

    public void setToken(String token) {
        this.token = token;
    }
    public void setId(Integer id) {
        this.id = id;
    }
}