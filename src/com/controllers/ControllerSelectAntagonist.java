package com.controllers;

import com.google.gson.Gson;
import com.services.CheckAuthService;
import com.services.SelectAntagonistService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

@ApplicationScoped
public class ControllerSelectAntagonist
{
    @Inject
    CheckAuthService checkAuthService;

    @Inject
    SelectAntagonistService selectAntagonistService;

    public Response get_antagonists(String json){
        Gson gson = new Gson();
        ObjectMethodBySelectAntagonist objectMethodBySelectAntagonist = gson.fromJson(json, ObjectMethodBySelectAntagonist.class);
        if (objectMethodBySelectAntagonist.getId() != null && objectMethodBySelectAntagonist.getToken() != null) {
            if (checkAuthService.check(objectMethodBySelectAntagonist.getId(), objectMethodBySelectAntagonist.getToken())){
                return selectAntagonistService.get_antagonists();
            }
        }
        return Response.status(401, "UNAUTHORISED").entity("Process error: Invalid token").build();
    }

    public Response select_antagonist(String json){
        Gson gson = new Gson();
        ObjectMethodBySelectAntagonist objectMethodBySelectAntagonist = gson.fromJson(json, ObjectMethodBySelectAntagonist.class);
        if (objectMethodBySelectAntagonist.getId() != null && objectMethodBySelectAntagonist.getToken() != null) {
            if (checkAuthService.check(objectMethodBySelectAntagonist.getId(), objectMethodBySelectAntagonist.getToken())){
                return selectAntagonistService.select_antagonist(objectMethodBySelectAntagonist.getId(), objectMethodBySelectAntagonist.getAntagonist_id());
            }
        }
        return Response.status(401, "UNAUTHORISED").entity("Process error: Invalid token").build();
    }
}

class ObjectMethodBySelectAntagonist
{
    private Integer id;
    private String token;
    private Integer antagonist_id;

    public Integer getId() {
        return id;
    }
    public String getToken() {
        return token;
    }
    public Integer getAntagonist_id() {
        return antagonist_id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public void setAntagonist_id(Integer antagonist_id) {
        this.antagonist_id = antagonist_id;
    }
}