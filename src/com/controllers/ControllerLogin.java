package com.controllers;

import com.google.gson.Gson;
import com.services.LoginService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

@ApplicationScoped
public class ControllerLogin
{
    @Inject
    LoginService loginService;

    public Response login(String json){
        Gson gson = new Gson();
        ObjectLogin objectLogin = gson.fromJson(json, ObjectLogin.class);
        return loginService.login(objectLogin.getLogin(),objectLogin.getPassword());
    }
}

class ObjectLogin
{
    private String login;
    private String password;

    public String getLogin() {
        return login;
    }
    public String getPassword() {
        return password;
    }

    public void setLogin(String login) {
        this.login = login;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
