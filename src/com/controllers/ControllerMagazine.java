package com.controllers;

import com.google.gson.Gson;
import com.services.CheckAuthService;
import com.services.ServiceMagazine;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

@ApplicationScoped
public class ControllerMagazine
{
    @Inject
    CheckAuthService checkAuthService;

    @Inject
    ServiceMagazine serviceMagazine;

    public Response get_assortment(String json){
        Gson gson = new Gson();
        ObjectMethodBuyAssortment objectMethodGetAssortments = gson.fromJson(json, ObjectMethodBuyAssortment.class);
        if (objectMethodGetAssortments.getId() != null && objectMethodGetAssortments.getToken() != null) {
            if (checkAuthService.check(objectMethodGetAssortments.getId(), objectMethodGetAssortments.getToken())){
                return serviceMagazine.get_assortment();
            }
        }
        return Response.status(401, "UNAUTHORISED").entity("Process error: Invalid token").build();
    }

    public Response buy_item(String json){
        Gson gson = new Gson();
        ObjectMethodBuyAssortment objectMethodBuyAssortment = gson.fromJson(json, ObjectMethodBuyAssortment.class);
        if (objectMethodBuyAssortment.getId() != null && objectMethodBuyAssortment.getToken() != null) {
            if (checkAuthService.check(objectMethodBuyAssortment.getId(), objectMethodBuyAssortment.getToken())){
                return serviceMagazine.buy_item(objectMethodBuyAssortment.getId(), objectMethodBuyAssortment.getItem_id(),objectMethodBuyAssortment.getItem_type());
            }
        }
        return Response.status(401, "UNAUTHORISED").entity("Process error: Invalid token").build();
    }
}

class ObjectMethodBuyAssortment
{
    private Integer id;
    private String token;
    private Integer item_id;
    private String item_type;

    public void setId(Integer id) {
        this.id = id;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public void setItem_id(Integer item_id) {
        this.item_id = item_id;
    }
    public void setItem_type(String item_type) {
        this.item_type = item_type;
    }

    public Integer getId() {
        return id;
    }
    public String getToken() {
        return token;
    }
    public Integer getItem_id() {
        return item_id;
    }
    public String getItem_type() {
        return item_type;
    }
}