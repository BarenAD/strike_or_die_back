package com.controllers;

import com.google.gson.Gson;
import com.services.RegistrationService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

@ApplicationScoped
public class ControllerRegistration
{
    @Inject
    RegistrationService registrationService;
    public Response registration(String json){
        Gson gson = new Gson();
        ObjectMethodRegistration objectMethodRegistration = gson.fromJson(json, ObjectMethodRegistration.class);
        return registrationService.registration(
                objectMethodRegistration.getName(),
                objectMethodRegistration.getAge(),
                objectMethodRegistration.getPassword(),
                objectMethodRegistration.getPhone()
        );
    }
}



class ObjectMethodRegistration
{
    private String name;
    private Integer age;
    private String password;
    private String phone;

    public String getName() {
        return name;
    }
    public Integer getAge() {
        return age;
    }
    public String getPassword() {
        return password;
    }
    public String getPhone() {
        return phone;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setAge(Integer age) {
        this.age = age;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
}