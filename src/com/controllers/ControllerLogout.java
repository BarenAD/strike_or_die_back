package com.controllers;

import com.google.gson.Gson;
import com.services.CheckAuthService;
import com.services.LogoutService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

@ApplicationScoped
public class ControllerLogout {

    @Inject
    CheckAuthService checkAuthService;

    @Inject
    LogoutService logoutService;

    public Response logout(String json)
    {
        Gson gson = new Gson();
        ObjectLogout objectLogout = gson.fromJson(json, ObjectLogout.class);
        if (objectLogout.getId() != null && objectLogout.getToken() != null) {
            if (checkAuthService.check(objectLogout.getId(), objectLogout.getToken())){
                return logoutService.logout(objectLogout.getId());
            }
        }
        return Response.status(401, "UNAUTHORISED").entity("Process error: Invalid token").build();
    }
}

class ObjectLogout
{
    private Integer id;
    private String token;

    public void setId(Integer id) {
        this.id = id;
    }
    public void setToken(String token) {
        this.token = token;
    }

    public Integer getId() {
        return id;
    }
    public String getToken() {
        return token;
    }
}