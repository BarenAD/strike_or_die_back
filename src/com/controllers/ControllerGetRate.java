package com.controllers;
import com.google.gson.Gson;
import com.services.CheckAuthService;
import com.services.GetRateService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

@ApplicationScoped
public class ControllerGetRate
{
    @Inject
    GetRateService getRateService;

    @Inject
    CheckAuthService checkAuthService;

    public Response getRate(String json)
    {
        Gson gson = new Gson();
        ObjectMethodRate objectMethodRate = gson.fromJson(json, ObjectMethodRate.class);
        if (objectMethodRate.getId() != null && objectMethodRate.getToken() != null) {
            if (checkAuthService.check(objectMethodRate.getId(), objectMethodRate.getToken())){
                return getRateService.analyzeAndGetRate(objectMethodRate.getId(),objectMethodRate.getAttack_area(),objectMethodRate.getBlock_area());
            }
        }
        return Response.status(401, "UNAUTHORISED").entity("Process error: Invalid token").build();
    }
}

class ObjectMethodRate
{
    private Integer id;
    private String token;
    private String attack_area;
    private String block_area;

    public String getAttack_area() {
        return attack_area;
    }
    public String getBlock_area() {
        return block_area;
    }
    public String getToken() {
        return token;
    }
    public Integer getId() {
        return id;
    }

    public void setAttack_area(String attack_area) {
        this.attack_area = attack_area;
    }
    public void setBlock_area(String block_area) {
        this.block_area = block_area;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public void setId(Integer id) {
        this.id = id;
    }
}