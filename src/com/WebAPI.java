package com;

import com.controllers.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api")
@ApplicationScoped
@ApplicationPath("/")
public class WebAPI extends Application
{
    @Inject
    ControllerGetRate controllerGetRate;

    @Inject
    ControllerGetSave controllerGetSave;

    @Inject
    ControllerLogin controllerLogin;

    @Inject
    ControllerRegistration controllerRegistration;

    @Inject
    ControllerMagazine controllerMagazine;

    @Inject
    ControllerSelectAntagonist controllerSelectAntagonist;

    @Inject
    ControllerLogout controllerLogout;

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(String json){
        return controllerLogin.login(json);
    }

    @POST
    @Path("/get_rate")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRate(String json){
        return controllerGetRate.getRate(json);
    }

    @POST
    @Path("/get_save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSave(String json) {
        return controllerGetSave.getSave(json);
    }

    @POST
    @Path("/registration")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response registration(String json) {
        return controllerRegistration.registration(json);
    }

    @POST
    @Path("/get_assortment_of_shops")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response get_assortment(String json){
        return controllerMagazine.get_assortment(json);
    }

    @POST
    @Path("/buy_item")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response buy_item(String json){
        return controllerMagazine.buy_item(json);
    }

    @POST
    @Path("/get_assortment_antagonists")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response get_antagonists(String json){
        return controllerSelectAntagonist.get_antagonists(json);
    }

    @POST
    @Path("/select_antagonist")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response select_antagonist(String json){
        return controllerSelectAntagonist.select_antagonist(json);
    }

    @POST
    @Path("/logout")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response logout(String json){
        return controllerLogout.logout(json);
    }
}