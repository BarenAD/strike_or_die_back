package com.database;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

public class DBEntityManager
{
    @PersistenceUnit(unitName = "pool_strike_or_die")
    private EntityManagerFactory entityManagerFactory;

    public EntityManager getEntityManager()
    {
        return this.entityManagerFactory.createEntityManager();
    }
}
