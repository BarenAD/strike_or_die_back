package com.database;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.sql.Connection;

public class DBConnection
{
    private Connection connection;
    private DataSource dataSource;

    public DBConnection()
    {
        try
        {
            InitialContext initialContext = new InitialContext();
            this.dataSource = (DataSource) initialContext.lookup("jdbc_strike_or_die_db");
        }
        catch(Exception e)
        {
            //return "JNDI: " + e.getMessage();
        }

        try
        {
            this.connection = this.dataSource.getConnection();
        }
        catch (Exception e)
        {
            //return "Connection error: " + e.getMessage();
        }
    }

    public Connection getConnection()
    {
        return this.connection;
    }
}
