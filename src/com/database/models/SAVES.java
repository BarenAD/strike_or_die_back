package com.database.models;
import javax.persistence.*;

@Entity
@Table(name = "\"SAVES\"")
public class SAVES {
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "\"ID\"")
    private Integer ID;
    @Column(name = "\"MONEY\"")
    private Integer MONEY;

    @OneToOne(optional = false)
    @JoinColumn(name="\"WEAPON_ID\"", updatable=true)
    private WEAPONS weapons;

    @OneToOne(optional = false)
    @JoinColumn(name="\"ARMOR_ID\"", updatable=true)
    private ARMORS armors;

    @OneToOne(optional = false)
    @JoinColumn(name="\"USER_ID\"", updatable=true)
    private USERS users;

    public Integer getID() {
        return ID;
    }
    public void setID(Integer ID) {
        this.ID = ID;
    }

    public USERS getUsers() {
        return users;
    }
    public void setUsers(USERS users) {
        this.users = users;
    }

    public WEAPONS getWeapons() {
        return weapons;
    }
    public void setWeapons(WEAPONS weapons) {
        this.weapons = weapons;
    }

    public ARMORS getArmors() {
        return armors;
    }
    public void setArmors(ARMORS armors) {
        this.armors = armors;
    }

    public Integer getMONEY() {
        return MONEY;
    }
    public void setMONEY(Integer MONEY) {
        this.MONEY = MONEY;
    }
}
