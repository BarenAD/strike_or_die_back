package com.database.models;

import javax.persistence.*;

@Entity
@Table(name = "\"ARMORS\"")
public class ARMORS
{
    @Id
    @Column(name = "\"ID\"")
    private Integer ID;
    @Column(name = "\"NAME\"")
    private String NAME;
    @Column(name = "\"DEFENSE\"")
    private Integer DEFENSE;
    @Column(name = "\"PRISE\"")
    private Integer PRISE;
    @Column(name = "\"SERVICE_NAME\"")
    private String SERVICE_NAME;

    @OneToOne(mappedBy="armors")
    private ANTAGONISTS antagonists;

    @OneToOne(mappedBy="armors")
    private SAVES saves;

    public ANTAGONISTS getAntagonists() {
        return antagonists;
    }
    public void setAntagonists(ANTAGONISTS antagonists) {
        this.antagonists = antagonists;
    }

    public SAVES getSaves() {
        return saves;
    }
    public void setSaves(SAVES saves) {
        this.saves = saves;
    }

    public Integer getID() {
        return ID;
    }
    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getNAME() {
        return NAME;
    }
    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public Integer getDEFENSE() {
        return DEFENSE;
    }
    public void setDEFENSE(Integer DEFENSE) {
        this.DEFENSE = DEFENSE;
    }

    public Integer getPRISE() {
        return PRISE;
    }
    public void setPRISE(Integer PRISE) {
        this.PRISE = PRISE;
    }

    public String getSERVICE_NAME() {
        return SERVICE_NAME;
    }
    public void setSERVICE_NAME(String SERVICE_NAME) {
        this.SERVICE_NAME = SERVICE_NAME;
    }
}
