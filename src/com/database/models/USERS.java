package com.database.models;

import javax.persistence.*;

@Entity
@Table(name = "\"USERS\"")
public class USERS {

    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "\"ID\"")
    private Integer ID;
    @Column(name = "\"NAME\"")
    private String NAME;
    @Column(name = "\"AGE\"")
    private Integer AGE;
    @Column(name = "\"PASSWORD\"")
    private String PASSWORD;
    @Column(name = "\"PHONE\"")
    private String PHONE;
    @Column(name = "\"TOKEN\"")
    private String TOKEN;

    @OneToOne(mappedBy="users")
    private SAVES saves;

    @OneToOne(mappedBy="users")
    private CURRENT_GAME current_game;

    public CURRENT_GAME getCurrent_game() {
        return current_game;
    }
    public void setCurrent_game(CURRENT_GAME current_game) {
        this.current_game = current_game;
    }

    public SAVES getSaves() {
        return saves;
    }
    public void setSaves(SAVES saves) {
        this.saves = saves;
    }

    public Integer getID() {
        return ID;
    }
    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getNAME() {
        return NAME;
    }
    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public Integer getAGE() {
        return AGE;
    }
    public void setAGE(Integer AGE) {
        this.AGE = AGE;
    }

    public String getPHONE() {
        return PHONE;
    }
    public void setPHONE(String PHONE) {
        this.PHONE = PHONE;
    }

    public String getTOKEN() {
        return TOKEN;
    }
    public void setTOKEN(String TOKEN) {
        this.TOKEN = TOKEN;
    }

    public String getPASSWORD() {
        return PASSWORD;
    }
    public void setPASSWORD(String PASSWORD) {
        this.PASSWORD = PASSWORD;
    }
}
