package com.database.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "\"CURRENT_GAME\"")
public class CURRENT_GAME implements Serializable
{
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "\"ID\"")
    private Integer ID;
    @Column(name = "\"HP_PLAYER\"")
    private Integer HP_PLAYER;
    @Column(name = "\"HP_COMPUTER\"")
    private Integer HP_COMPUTER;

    @OneToOne(optional = false)
    @JoinColumn(name="\"USER_ID\"", updatable=true)
    private USERS users;

    @OneToOne(optional = false)
    @JoinColumn(name="\"ANTAGONIST_ID\"", nullable=false, updatable=true)
    private ANTAGONISTS antagonists;

    public USERS getUsers() {
        return users;
    }
    public void setUsers(USERS users) {
        this.users = users;
    }

    public ANTAGONISTS getAntagonists() {
        return antagonists;
    }
    public void setAntagonists(ANTAGONISTS inAntagonists) {
        this.antagonists = inAntagonists;
    }

    public Integer getID() {
        return ID;
    }
    public void setID(Integer inID) {
        ID = inID;
    }

    public Integer getHP_PLAYER() {
        return HP_PLAYER;
    }
    public void setHP_PLAYER(Integer inHP) {
        HP_PLAYER = inHP;
    }

    public Integer getHP_COMPUTER() {
        return HP_COMPUTER;
    }
    public void setHP_COMPUTER(Integer inHP) {
        HP_COMPUTER = inHP;
    }
}
