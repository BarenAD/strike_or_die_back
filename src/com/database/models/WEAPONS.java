package com.database.models;

import javax.persistence.*;

@Entity
@Table(name = "\"WEAPONS\"")
public class WEAPONS
{
    @Id
    @Column(name = "\"ID\"")
    private Integer ID;
    @Column(name = "\"NAME\"")
    private String NAME;
    @Column(name = "\"DAMAGE\"")
    private Integer DAMAGE;
    @Column(name = "\"PRISE\"")
    private Integer PRISE;
    @Column(name = "\"SERVICE_NAME\"")
    private String SERVICE_NAME;

    @OneToOne(mappedBy="weapons")
    public ANTAGONISTS antagonists;

    @OneToOne(mappedBy="weapons")
    public SAVES saves;

    public ANTAGONISTS getAntagonists() {
        return antagonists;
    }
    public void setAntagonists(ANTAGONISTS antagonists) {
        this.antagonists = antagonists;
    }

    public SAVES getSaves() {
        return saves;
    }
    public void setSaves(SAVES saves) {
        this.saves = saves;
    }

    public Integer getID() {
        return ID;
    }
    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getNAME() {
        return NAME;
    }
    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public Integer getDAMAGE() {
        return DAMAGE;
    }
    public void setDAMAGE(Integer DAMAGE) {
        this.DAMAGE = DAMAGE;
    }

    public Integer getPRISE() {
        return PRISE;
    }
    public void setPRISE(Integer PRISE) {
        this.PRISE = PRISE;
    }

    public String getSERVICE_NAME() {
        return SERVICE_NAME;
    }
    public void setSERVICE_NAME(String SERVICE_NAME) {
        this.SERVICE_NAME = SERVICE_NAME;
    }
}
