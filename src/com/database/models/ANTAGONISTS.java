package com.database.models;

import javax.persistence.*;

@Entity
@Table(name = "\"ANTAGONISTS\"")
public class ANTAGONISTS
{
    @Id
    @Column(name = "\"ID\"")
    private Integer ID;
    @Column(name = "\"NAME\"")
    private String NAME;
    @Column(name = "\"PRIZE\"")
    private Integer PRIZE;

    @OneToOne(optional = false)
    @JoinColumn(name="\"WEAPON_ID\"", updatable=true)
    private WEAPONS weapons;

    @OneToOne(optional = false)
    @JoinColumn(name="\"ARMOR_ID\"", updatable=true)
    private ARMORS armors;

    @OneToOne(mappedBy="antagonists")
    private CURRENT_GAME current_game;

    public CURRENT_GAME getCurrent_game() {
        return current_game;
    }
    public void setCurrent_game(CURRENT_GAME inCurrent_game) {
        this.current_game = inCurrent_game;
    }

    public WEAPONS getWeapons() {
        return weapons;
    }
    public void setWeapons(WEAPONS weapons) {
        this.weapons = weapons;
    }

    public ARMORS getArmors() {
        return armors;
    }
    public void setArmors(ARMORS armors) {
        this.armors = armors;
    }

    public Integer getID() {
        return ID;
    }
    public void setID(Integer inID) {
        ID = inID;
    }

    public String getNAME() {
        return NAME;
    }
    public void setNAME(String inNAME) {
        NAME = inNAME;
    }

    public Integer getPRIZE() {
        return PRIZE;
    }

    public void setPRIZE(Integer PRIZE) {
        this.PRIZE = PRIZE;
    }
}
