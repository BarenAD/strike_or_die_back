package com.services;

import com.database.DBEntityManager;
import com.database.models.USERS;
import com.google.gson.Gson;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.Response;

@ApplicationScoped
public class LogoutService {

    @Inject
    DBEntityManager dbEntityManager;

    @Resource
    private UserTransaction userTransaction;

    public Response logout(Integer id){
        EntityManager entityManager = dbEntityManager.getEntityManager();
        try {
            userTransaction.begin();
            entityManager.joinTransaction();
            USERS users = entityManager.find(USERS.class,id);
            users.setTOKEN(null);
            entityManager.merge(users);
            userTransaction.commit();
        }
        catch (Exception e) {
            return Response.status(500, "(JPA)").entity("Process error: " + e.getMessage()).build();
        }
        Gson gson = new Gson();
        String jsonResponse = gson.toJson("LOGOUT");
        return  Response.ok(jsonResponse).build();
    }
}
