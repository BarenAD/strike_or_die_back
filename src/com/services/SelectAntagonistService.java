package com.services;

import com.database.DBConnection;
import com.database.DBEntityManager;
import com.database.models.ANTAGONISTS;
import com.database.models.CURRENT_GAME;
import com.database.models.USERS;
import com.google.gson.Gson;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.Response;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class SelectAntagonistService
{
    @Inject
    DBEntityManager dbEntityManager;

    @Inject
    DBConnection dbConnection;

    @Resource
    private UserTransaction userTransaction;

    public Response get_antagonists(){
        List<Integer> list_ids = new ArrayList<Integer>();
        List<ResponseObjectAntagonists> responseObjectAntagonists = new ArrayList<ResponseObjectAntagonists>();
        Connection connection = dbConnection.getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT ID FROM ANTAGONISTS");
            while (resultSet.next()){
                list_ids.add(resultSet.getInt("ID"));
            }
            resultSet.close();
            statement.close();
        }
        catch (Exception e) {
            return Response.status(500, "JPA").entity("Process error: " + e.getMessage()).build();
        }
        try {
            EntityManager entityManager = dbEntityManager.getEntityManager();
            userTransaction.begin();
            entityManager.joinTransaction();
            ANTAGONISTS antagonists;
            for (Integer temp_id : list_ids){
                ResponseObjectAntagonists antagonist_temp = new ResponseObjectAntagonists();
                antagonists = entityManager.find(ANTAGONISTS.class,temp_id);
                antagonist_temp.setId(antagonists.getID());
                if (antagonists.getNAME() != null){
                    antagonist_temp.setName(antagonists.getNAME());
                } else {
                    antagonist_temp.setName("без имени");
                }
                if (antagonists.getArmors() != null){
                    antagonist_temp.setArmor(antagonists.getArmors().getNAME());
                } else {
                    antagonist_temp.setArmor("без брони");
                }
                if (antagonists.getWeapons() != null){
                    antagonist_temp.setWeapon(antagonists.getWeapons().getNAME());
                } else {
                    antagonist_temp.setWeapon("без оружия");
                }
                if (antagonists.getPRIZE() != null){
                    antagonist_temp.setPrize(antagonists.getPRIZE());
                } else {
                    antagonist_temp.setPrize(0);
                }
                responseObjectAntagonists.add(antagonist_temp);
            }
            entityManager.close();
            userTransaction.commit();
        }
        catch (Exception e) {
            return Response.status(500, "JPA").entity("Process error: " + e.getMessage()).build();
        }
        Gson gson = new Gson();
        String jsonResponse = gson.toJson(responseObjectAntagonists);
        return  Response.ok(jsonResponse).build();
    }

    public Response select_antagonist(Integer user_id, Integer antagonist_id){
        ResponseSelectObjectAntagonist selectObjectAntagonist = new ResponseSelectObjectAntagonist();
        try {
            EntityManager entityManager = dbEntityManager.getEntityManager();
            userTransaction.begin();
            entityManager.joinTransaction();
            USERS users = entityManager.find(USERS.class,user_id);
            ANTAGONISTS antagonists = entityManager.find(ANTAGONISTS.class,antagonist_id);
            CURRENT_GAME new_current_game = new CURRENT_GAME();
            new_current_game.setUsers(users);
            new_current_game.setAntagonists(antagonists);
            new_current_game.setHP_PLAYER(100);
            new_current_game.setHP_COMPUTER(100);
            users.setCurrent_game(new_current_game);
            entityManager.persist(new_current_game);
            userTransaction.commit();
            if (antagonists.getArmors() != null){
                selectObjectAntagonist.setArmor(antagonists.getArmors().getSERVICE_NAME());
            }
            if (antagonists.getWeapons() != null){
                selectObjectAntagonist.setWeapon(antagonists.getWeapons().getSERVICE_NAME());
            }
            Gson gson = new Gson();
            String jsonResponse = gson.toJson(selectObjectAntagonist);
            return  Response.ok(jsonResponse).build();
        }
        catch (Exception e) {
            return Response.status(500, "JPA").entity("Process error: " + e.getMessage()).build();
        }
    }
}

class ResponseSelectObjectAntagonist
{
    private String armor = null;
    private String weapon = null;

    public String getWeapon() {
        return weapon;
    }
    public String getArmor() {
        return armor;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }
    public void setArmor(String armor) {
        this.armor = armor;
    }
}

class ResponseObjectAntagonists
{
    private Integer id;
    private String name;
    private String armor;
    private String weapon;
    private Integer prize;

    public void setId(Integer id) {
        this.id = id;
    }
    public void setArmor(String armor) {
        this.armor = armor;
    }
    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }
    public void setPrize(Integer prize) {
        this.prize = prize;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }
    public String getArmor() {
        return armor;
    }
    public String getWeapon() {
        return weapon;
    }
    public Integer getPrize() {
        return prize;
    }
    public String getName() {
        return name;
    }
}