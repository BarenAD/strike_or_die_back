package com.services;

import com.database.DBConnection;
import com.database.DBEntityManager;
import com.database.models.ARMORS;
import com.database.models.SAVES;
import com.database.models.USERS;
import com.database.models.WEAPONS;
import com.google.gson.Gson;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.Response;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class ServiceMagazine
{
    @Inject
    DBEntityManager dbEntityManager;

    @Inject
    DBConnection dbConnection;

    @Resource
    private UserTransaction userTransaction;

    public Response get_assortment(){
        List<ResponseAssortimentObject> responseAssortimentObject = new ArrayList<ResponseAssortimentObject>();
        Connection connection = dbConnection.getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM ARMORS");
            while (resultSet.next()){
                ResponseAssortimentObject temp = new ResponseAssortimentObject();
                temp.setId(resultSet.getInt("ID"));
                temp.setName(resultSet.getString("NAME"));
                temp.setPrise(resultSet.getInt("PRISE"));
                temp.setItem_type("armor");
                temp.setService_name(resultSet.getString("SERVICE_NAME"));
                responseAssortimentObject.add(temp);
            }
            resultSet.close();
            resultSet = statement.executeQuery("SELECT * FROM WEAPONS");
            while (resultSet.next()){
                ResponseAssortimentObject temp = new ResponseAssortimentObject();
                temp.setId(resultSet.getInt("ID"));
                temp.setName(resultSet.getString("NAME"));
                temp.setPrise(resultSet.getInt("PRISE"));
                temp.setItem_type("weapon");
                temp.setService_name(resultSet.getString("SERVICE_NAME"));
                responseAssortimentObject.add(temp);
            }
            resultSet.close();
            statement.close();
        }
        catch (Exception e) {
            return Response.status(500, "JPA").entity("Process error: " + e.getMessage()).build();
        }
        Gson gson = new Gson();
        String jsonResponse = gson.toJson(responseAssortimentObject);
        return  Response.ok(jsonResponse).build();
    }

    public Response buy_item(Integer id, Integer item_id, String item_type){
        EntityManager entityManager = dbEntityManager.getEntityManager();
        try {
            userTransaction.begin();
            entityManager.joinTransaction();
            USERS users = entityManager.find(USERS.class,id);
            Integer userMoney = users.getSaves().getMONEY();
            Boolean successfulTransaction = false;
            Connection connection = dbConnection.getConnection();
            try {
                Statement statement = connection.createStatement();
                if (item_type.equals("armor")){
                    ARMORS see_armor = entityManager.find(ARMORS.class,item_id);
                    if (users.getSaves().getArmors() != null){
                        userMoney += (users.getSaves().getArmors().getPRISE() / 2);
                    }
                    userMoney -= see_armor.getPRISE();
                    if (userMoney >= 0){
                        users.getSaves().setArmors(see_armor);
                        statement.executeUpdate("UPDATE SAVES SET ARMOR_ID = " + see_armor.getID() + " WHERE ID = " + users.getSaves().getID());
                        users.getSaves().setMONEY(userMoney);
                        successfulTransaction = true;
                    }
                } else if (item_type.equals("weapon")) {
                    WEAPONS see_weapon = entityManager.find(WEAPONS.class,item_id);
                    if (users.getSaves().getWeapons() != null){
                        userMoney += (users.getSaves().getWeapons().getPRISE() / 2);
                    }
                    userMoney -= see_weapon.getPRISE();
                    if (userMoney >= 0){
                        users.getSaves().setWeapons(see_weapon);
                        statement.executeUpdate("UPDATE SAVES SET WEAPON_ID = " + see_weapon.getID() + " WHERE ID = " + users.getSaves().getID());
                        users.getSaves().setMONEY(userMoney);
                        successfulTransaction = true;
                    }
                }
                statement.close();
            }
            catch (Exception e) {
                return Response.status(500, "JPA").entity("Process error: " + e.getMessage()).build();
            }
            if (successfulTransaction){
                entityManager.merge(users);
                userTransaction.commit();
                Gson gson = new Gson();
                SooGOOD sooGOOD = new SooGOOD();
                sooGOOD.setGood("successful transaction!");
                String jsonResponse = gson.toJson(sooGOOD);
                return  Response.ok(jsonResponse).build();
            } else {
                entityManager.close();
                userTransaction.commit();
                return Response.status(400, "NO MONEY").entity("infoNO MONEY or invalid item_type or invalid item_id").build();
            }
        }
        catch (Exception e) {
            return Response.status(500, "JPA").entity("Process error: " + e.getMessage()).build();
        }
    }
}

class SooGOOD
{
    private String good;

    public String getGood() {
        return good;
    }
    public void setGood(String good) {
        this.good = good;
    }
}

class ResponseAssortimentObject
{
    private Integer id;
    private String item_type;
    private String name;
    private Integer prise;
    private String service_name;

    public Integer getId() {
        return id;
    }
    public String getItem_type() {
        return item_type;
    }
    public String getName() {
        return name;
    }
    public Integer getPrise() {
        return prise;
    }
    public String getService_name() {
        return service_name;
    }

    public void setItem_type(String item_type) {
        this.item_type = item_type;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setPrise(Integer prise) {
        this.prise = prise;
    }
    public void setService_name(String service_name) {
        this.service_name = service_name;
    }
}