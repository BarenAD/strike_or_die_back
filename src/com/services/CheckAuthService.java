package com.services;

import com.database.DBEntityManager;
import com.database.models.USERS;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

@ApplicationScoped
public class CheckAuthService
{
    @Inject
    DBEntityManager dbEntityManager;

    @Resource
    private UserTransaction userTransaction;

    public Boolean check(Integer id, String in_token)
    {
        EntityManager entityManager = dbEntityManager.getEntityManager();
        try {
            userTransaction.begin();
            entityManager.joinTransaction();
            USERS users = entityManager.find(USERS.class,id);
            if (users != null){
                String token = users.getTOKEN();
                if (in_token.equals(token)) {
                    userTransaction.commit();
                    return true;
                }
            }
            entityManager.close();
            userTransaction.commit();
        }
        catch (Exception e) {
            return null;
        }
        return false;
    }
}
