package com.services;

import com.database.DBEntityManager;
import com.database.models.ANTAGONISTS;
import com.database.models.CURRENT_GAME;
import com.database.models.SAVES;
import com.database.models.USERS;
import com.google.gson.Gson;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.Response;

@ApplicationScoped
public class RegistrationService
{
    @Inject
    DBEntityManager dbEntityManager;

    @Inject
    LoginService loginService;

    @Resource
    private UserTransaction userTransaction;

    public Response registration(String name, Integer age, String password, String phone){
        EntityManager entityManager = dbEntityManager.getEntityManager();
        try {
            userTransaction.begin();
            entityManager.joinTransaction();
            USERS new_user = new USERS();
            new_user.setNAME(name);
            new_user.setAGE(age);
            new_user.setPASSWORD(password);
            new_user.setPHONE(phone);
            entityManager.persist(new_user);
            SAVES new_saves = new SAVES();
            new_saves.setWeapons(null);
            new_saves.setArmors(null);
            new_saves.setMONEY(1000);
            new_saves.setUsers(new_user);
            new_user.setSaves(new_saves);
            entityManager.persist(new_saves);
            userTransaction.commit();
            return loginService.login(new_user.getPHONE(), new_user.getPASSWORD());
        }
        catch (Exception e) {
            return Response.status(500, "JPA").entity("Process error: " + e.getMessage()).build();
        }
    }
}
