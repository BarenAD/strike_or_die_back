package com.services;

import com.database.DBEntityManager;
import com.database.models.USERS;
import com.google.gson.Gson;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.Response;
import java.util.Random;

@ApplicationScoped
public class GetRateService
{
    @Inject
    DBEntityManager dbEntityManager;

    @Resource
    private UserTransaction userTransaction;

    private String[] areas = {"head", "tors", "legs"};

    private Boolean ValidateAttackData(String attack_area){
        for (String area : areas)
        {
            if (attack_area.equals(area))
            {
                return true;
            }
        }
        return false;
    }

    public Response analyzeAndGetRate(Integer user_id, String attack_area, String block_area)
    {
        ResponseObject responseObject = new ResponseObject();
        Random random = new Random();
        EntityManager entityManager = dbEntityManager.getEntityManager();
        String ComputerAttackArea = areas[random.nextInt(2)];
        String ComputerBlockArea = areas[random.nextInt(2)];

        responseObject.setSuccessful_attack(false);
        if (this.ValidateAttackData(attack_area)) {
            if (!attack_area.equals(ComputerBlockArea)) {
                responseObject.setSuccessful_attack(true);
            }
        }
        responseObject.setSuccessful_block(false);
        if (ComputerAttackArea.equals(block_area)) {
            responseObject.setSuccessful_block(true);
        }
        try {
            userTransaction.begin();
            entityManager.joinTransaction();
            USERS users = entityManager.find(USERS.class,user_id);
            if (users.getCurrent_game() != null) {
                Integer PlayerDefense = 0;
                Integer PlayerDamage = 10;
                if (users.getSaves().getArmors() != null){
                    PlayerDefense = users.getSaves().getArmors().getDEFENSE();
                }
                if (users.getSaves().getWeapons() != null){
                    PlayerDamage = users.getSaves().getWeapons().getDAMAGE();
                }
                Integer PlayerHP = users.getCurrent_game().getHP_PLAYER();
                Integer PlayerMoney = users.getSaves().getMONEY();
                Integer ComputerHP = users.getCurrent_game().getHP_COMPUTER();
                Integer ComputerDefense = 0;
                Integer ComputerDamage = 10;
                if (users.getCurrent_game().getAntagonists().getArmors() != null){
                    ComputerDefense = users.getCurrent_game().getAntagonists().getArmors().getDEFENSE();
                }
                if (users.getCurrent_game().getAntagonists().getWeapons() != null){
                    ComputerDamage = users.getCurrent_game().getAntagonists().getWeapons().getDAMAGE();
                }

                Integer DealDamage = 0;
                if (responseObject.getSuccessful_attack()) {
                    DealDamage = PlayerDamage - ComputerDefense;
                } else {
                    DealDamage = PlayerDamage / 2 - ComputerDefense;
                }
                if (DealDamage < 0){
                    DealDamage = 0;
                }
                ComputerHP -= DealDamage;
                if (responseObject.getSuccessful_block()) {
                    DealDamage = ComputerDamage / 2 - PlayerDefense;
                } else {
                    DealDamage = ComputerDamage - PlayerDefense;
                }
                if (DealDamage < 0){
                    DealDamage = 0;
                }
                PlayerHP -= DealDamage;
                responseObject.setComputerHP(ComputerHP);
                responseObject.setPlayerHP(PlayerHP);
                if (ComputerHP <= 0) {
                    PlayerMoney += users.getCurrent_game().getAntagonists().getPRIZE();
                    responseObject.setEnd_game("player");
                    responseObject.setPrize(users.getCurrent_game().getAntagonists().getPRIZE());
                    users.getSaves().setMONEY(PlayerMoney);

                } else if (PlayerHP <= 0) {
                    PlayerMoney -= users.getCurrent_game().getAntagonists().getPRIZE();
                    if (PlayerMoney < 0) {
                        PlayerMoney = 0;
                    }
                    users.getSaves().setMONEY(PlayerMoney);
                    responseObject.setEnd_game("computer");
                    responseObject.setPrize(0 - users.getCurrent_game().getAntagonists().getPRIZE());
                } else {
                    users.getCurrent_game().setHP_COMPUTER(ComputerHP);
                    users.getCurrent_game().setHP_PLAYER(PlayerHP);
                }
                entityManager.merge(users);
                if (ComputerHP <= 0 || PlayerHP <= 0){
                    entityManager.remove(users.getCurrent_game());
                    users.setCurrent_game(null);
                }
                entityManager.close();
                userTransaction.commit();
            } else {
                entityManager.merge(users);
                entityManager.close();
                userTransaction.commit();
                return Response.status(500, "TABLE").entity("TABLE CURRENT_GAME IS NOT FOUND").build();
            }
        }
        catch (Exception e) {
            return Response.status(500, "JPA").entity("Process error: " + e.getMessage()).build();
        }
        Gson gson = new Gson();
        String jsonResponse = gson.toJson(responseObject);
        return  Response.ok(jsonResponse).build();
    }
}

class ResponseObject
{
    private Boolean successful_attack;
    private Boolean successful_block;
    private Integer PlayerHP;
    private Integer ComputerHP;
    private String end_game = null;
    private Integer prize = null;

    public Boolean getSuccessful_attack() {
        return successful_attack;
    }
    public Boolean getSuccessful_block() {
        return successful_block;
    }
    public Integer getComputerHP() {
        return ComputerHP;
    }
    public Integer getPlayerHP() {
        return PlayerHP;
    }
    public Integer getPrize() {
        return prize;
    }
    public String getEnd_game() {
        return end_game;
    }

    public void setComputerHP(Integer computerHP) {
        ComputerHP = computerHP;
    }
    public void setEnd_game(String end_game) {
        this.end_game = end_game;
    }
    public void setPlayerHP(Integer playerHP) {
        PlayerHP = playerHP;
    }
    public void setPrize(Integer prize) {
        this.prize = prize;
    }
    public void setSuccessful_attack(Boolean successful_attack) {
        this.successful_attack = successful_attack;
    }
    public void setSuccessful_block(Boolean successful_block) {
        this.successful_block = successful_block;
    }
}