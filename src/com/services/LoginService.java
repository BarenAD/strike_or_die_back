package com.services;

import com.database.DBConnection;
import com.database.DBEntityManager;
import com.database.models.USERS;
import com.google.gson.Gson;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.Response;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

@ApplicationScoped
public class LoginService
{
    @Inject
    DBConnection dbConnection;

    @Inject
    DBEntityManager dbEntityManager;

    @Resource
    private UserTransaction userTransaction;

    public Response login(String phone, String password)
    {
        ObjectResponse objectResponse = new ObjectResponse();
        Connection connection = dbConnection.getConnection();
        EntityManager entityManager = dbEntityManager.getEntityManager();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM USERS WHERE PHONE LIKE '"+phone+"'");
            String PASSWORD = "";
            while (resultSet.next()){
                objectResponse.setId(resultSet.getInt("ID"));
                PASSWORD = resultSet.getString("PASSWORD");
            }
            resultSet.close();
            statement.close();
            if (PASSWORD.equals(password)){
                SecureRandom random = new SecureRandom();
                byte bytes[] = new byte[50];
                random.nextBytes(bytes);
                objectResponse.setToken(bytes.toString());
//                statement = connection.createStatement();
//                statement.executeUpdate("UPDATE USERS SET TOKEN = '" + objectResponse.getToken() + "' WHERE ID = " + objectResponse.getId());
//                resultSet.close();
//                statement.close();
                userTransaction.begin();
                entityManager.joinTransaction();
                USERS users = entityManager.find(USERS.class, objectResponse.getId());
                users.setTOKEN(objectResponse.getToken());
                entityManager.merge(users);
                entityManager.close();
                userTransaction.commit();
            } else {
                return Response.status(401, "UNAUTORIZED").entity("Invalid login or password").build();
            }
        }
        catch (Exception e) {
            return Response.status(500, "JPA").entity("Process error: " + e.getMessage()).build();
        }
        Gson gson = new Gson();
        String jsonResponse = gson.toJson(objectResponse);
        return  Response.ok(jsonResponse).build();
    }
}

class ObjectResponse
{
    private Integer id;
    private String token;

    public String getToken() {
        return token;
    }
    public Integer getId() {
        return id;
    }

    public void setToken(String token) {
        this.token = token;
    }
    public void setId(Integer id) {
        this.id = id;
    }
}
