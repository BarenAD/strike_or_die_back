package com.services;

import com.database.DBEntityManager;
import com.database.models.SAVES;
import com.database.models.USERS;
import com.google.gson.Gson;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.Response;

@ApplicationScoped
public class GetSaveService
{
    @Inject
    DBEntityManager dbEntityManager;

    @Resource
    private UserTransaction userTransaction;

    public Response getSave(Integer id)
    {
        EntityManager entityManager = dbEntityManager.getEntityManager();
        ResponseObjectGetSave responseObjectGetSave = new ResponseObjectGetSave();
        try {
            userTransaction.begin();
            entityManager.joinTransaction();
            USERS users = entityManager.find(USERS.class,id);
            if (users.getSaves().getArmors() != null){
                responseObjectGetSave.setPlayer_armor(users.getSaves().getArmors().getSERVICE_NAME());
            }
            if (users.getSaves().getWeapons() != null){
                responseObjectGetSave.setPlayer_weapon(users.getSaves().getWeapons().getSERVICE_NAME());
            }
            responseObjectGetSave.setPlayer_money(users.getSaves().getMONEY());
            if (users.getCurrent_game() != null){
                responseObjectGetSave.setOld_game(true);
                responseObjectGetSave.setPlayer_hp(users.getCurrent_game().getHP_PLAYER());
                responseObjectGetSave.setComputer_hp(users.getCurrent_game().getHP_COMPUTER());
                if (users.getCurrent_game().getAntagonists().getArmors() != null){
                    responseObjectGetSave.setComputer_armor(users.getCurrent_game().getAntagonists().getArmors().getSERVICE_NAME());
                }
                if (users.getCurrent_game().getAntagonists().getWeapons() != null){
                    responseObjectGetSave.setComputer_weapon(users.getCurrent_game().getAntagonists().getWeapons().getSERVICE_NAME());
                }
            }
            userTransaction.commit();
        }
        catch (Exception e) {
            return Response.status(500, "(JPA)").entity("Process error: " + e.getMessage()).build();
        }
        Gson gson = new Gson();
        String jsonResponse = gson.toJson(responseObjectGetSave);
        return  Response.ok(jsonResponse).build();
    }
}

class ResponseObjectGetSave
{
    private String player_armor = null;
    private String player_weapon = null;
    private Integer player_money;
    private Boolean old_game = false;
    private Integer player_hp;
    private Integer computer_hp;
    private String computer_armor = null;
    private String computer_weapon = null;

    public String getPlayer_armor() {
        return player_armor;
    }
    public String getPlayer_weapon() {
        return player_weapon;
    }
    public Integer getPlayer_money() {
        return player_money;
    }
    public Boolean getOld_game() {
        return old_game;
    }
    public Integer getPlayer_hp() {
        return player_hp;
    }
    public Integer getComputer_hp() {
        return computer_hp;
    }
    public String getComputer_armor() {
        return computer_armor;
    }
    public String getComputer_weapon() {
        return computer_weapon;
    }

    public void setPlayer_armor(String player_armor) {
        this.player_armor = player_armor;
    }
    public void setPlayer_weapon(String player_weapon) {
        this.player_weapon = player_weapon;
    }
    public void setPlayer_money(Integer player_money) {
        this.player_money = player_money;
    }
    public void setOld_game(Boolean old_game) {
        this.old_game = old_game;
    }
    public void setPlayer_hp(Integer player_hp) {
        this.player_hp = player_hp;
    }
    public void setComputer_hp(Integer computer_hp) {
        this.computer_hp = computer_hp;
    }
    public void setComputer_armor(String computer_armor) {
        this.computer_armor = computer_armor;
    }
    public void setComputer_weapon(String computer_weapon) {
        this.computer_weapon = computer_weapon;
    }
}